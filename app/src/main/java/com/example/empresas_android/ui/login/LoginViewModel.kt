package com.example.empresas_android.ui.login

import android.util.Log
import android.view.View
import androidx.databinding.BindingAdapter
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.empresas_android.generated.callback.OnClickListener
import com.example.empresas_android.models.AuthenticatedUser
import com.example.empresas_android.models.User
import com.example.empresas_android.repository.UserRepository
import kotlinx.coroutines.launch
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class LoginViewModel : ViewModel(), KoinComponent {

    private val userRepository: UserRepository by inject()
    private val user: User by inject()

    private val _areFieldsValid = MutableLiveData<Boolean>()
    private val _isLoading = MutableLiveData<Boolean>()
    private val _isButtonEnabled = MutableLiveData<Boolean>()
    private val _isEmailValid = MutableLiveData<Boolean>()
    private val _isPasswordValid = MutableLiveData<Boolean>()

    val _authenticatedUser = MutableLiveData<AuthenticatedUser>()

    private val _userData = MutableLiveData<User>()
    private val _isUserNeeded = MutableLiveData<Boolean>()
    val isUserNeeded: LiveData<Boolean>
        get() = _isUserNeeded

    private val _hasActiveWarningInPassword = MutableLiveData<Boolean>()
    val hasActiveWarningInPassword: LiveData<Boolean>
        get() = _hasActiveWarningInPassword

    private val _shouldShowInvalidFieldsWarning = MutableLiveData<Boolean>()
    val shouldShowInvalidFieldsWarning: LiveData<Boolean>
        get() = _shouldShowInvalidFieldsWarning

    val areFieldsValid: LiveData<Boolean>
        get() = _areFieldsValid
    val isLoading: LiveData<Boolean>
        get() = _isLoading
    val isEmailValid: LiveData<Boolean>
        get() = _isEmailValid
    val isPasswordValid: LiveData<Boolean>
        get() = _isPasswordValid

    fun setIsButtonEnabled(condition: Boolean){
        _isButtonEnabled.postValue(condition)
    }

    fun setIsLoading(condition: Boolean){
        _isLoading.postValue(condition)
    }

    fun setAreFieldsValid(condition: Boolean){
        _areFieldsValid.postValue(condition)
    }

    fun setIsEmailValid(condition: Boolean){
        _isEmailValid.value = condition
    }

    fun setIsPasswordValid(condition: Boolean){
        _isPasswordValid.value = condition
    }

    fun setAreFieldsValid(){
        _areFieldsValid.value = (_isPasswordValid.value == true  && _isEmailValid.value == true)
    }

    fun setShouldShowInvalidFieldsWarning(condition: Boolean) {
        _shouldShowInvalidFieldsWarning.value = condition
    }

    fun setShouldShowInvalidFieldsWarning(){
        _shouldShowInvalidFieldsWarning.value = (_isPasswordValid.value == false || _isEmailValid.value == false)
    }

    fun setHasActiveWarningInPassword(condition: Boolean){
        _hasActiveWarningInPassword.value = condition
    }

    fun setUserData(user: User){
        _userData.value = user
    }

    fun onLoginClick(){
        _isUserNeeded.value = true
        _isLoading.value = true
        viewModelScope.launch {
            _authenticatedUser.value = _userData.value?.let { userRepository.login(it) }
        }
    }
}
