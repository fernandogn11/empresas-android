package com.example.empresas_android.ui.search

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.empresas_android.models.AuthenticatedUser
import com.example.empresas_android.models.Enterprise
import com.example.empresas_android.models.Enterprises
import com.example.empresas_android.repository.SearchRepository
import kotlinx.coroutines.launch
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class SearchViewModel : ViewModel(), KoinComponent {
    private val searchRepository : SearchRepository by inject()

    private val _enterpriseList = MutableLiveData<List<Enterprise>>()
    val enterpriseList : LiveData<List<Enterprise>>
    get() = _enterpriseList

    fun handleSearchRequest(query: String){
        viewModelScope.launch {
            searchRepository.search(query)?.enterpriseList?.let {
                _enterpriseList.value = it
            }
        }
    }
}