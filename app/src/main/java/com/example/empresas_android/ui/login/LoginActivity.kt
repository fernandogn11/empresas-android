package com.example.empresas_android.ui.login

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doAfterTextChanged
import androidx.databinding.DataBindingUtil
import com.example.empresas_android.App.Companion.saveAuthenticatedUserData
import com.example.empresas_android.R
import com.example.empresas_android.databinding.ActivityLoginBinding
import com.example.empresas_android.models.User
import com.example.empresas_android.ui.search.SearchActivity
import com.google.android.material.textfield.TextInputLayout
import com.google.android.material.textfield.TextInputLayout.*

class LoginActivity : AppCompatActivity() {
    private val loginViewModel: LoginViewModel by viewModels()

    private lateinit var loginDataBinding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        loginDataBinding =  DataBindingUtil.setContentView(this, R.layout.activity_login)
        loginDataBinding.viewModel = loginViewModel
        setListenersToEmailField()
        setListenersToPasswordField()
        setObservers()
        super.onCreate(savedInstanceState)
    }

    private fun setObservers(){
        loginViewModel._authenticatedUser.observe(this){
            if(it != null){
                saveAuthenticatedUserData(it)
                moveToSearchActivity()
            } else {
                loginViewModel.setShouldShowInvalidFieldsWarning(true)
                loginViewModel.setIsLoading(false)
            }
        }
        loginViewModel.isLoading.observe(this){
            if(it == true){
                loginDataBinding.loadingScreen.root.visibility = VISIBLE
                loginDataBinding.loginButton.isEnabled = false
            } else {
                loginDataBinding.loadingScreen.root.visibility = GONE
                loginDataBinding.loginButton.isEnabled = true
            }
        }
        loginViewModel.isUserNeeded.observe(this){
            if(it == true)
                loginViewModel.setUserData(createUser())
        }

        loginViewModel.areFieldsValid.observe(this){
            loginDataBinding.loginButton.isEnabled = it
            changeButtonColorsScheme(it)
        }

        loginViewModel.hasActiveWarningInPassword.observe(this){
            if(it == false){
                if(loginDataBinding.passwordEditText.endIconMode != END_ICON_PASSWORD_TOGGLE)
                    loginDataBinding.passwordEditText.endIconMode = END_ICON_PASSWORD_TOGGLE
            }
        }

        loginViewModel.shouldShowInvalidFieldsWarning.observe(this){
            if(it == true){
                loginDataBinding.errorMessageTextView.visibility = VISIBLE
            } else {
                loginDataBinding.errorMessageTextView.visibility = GONE
            }
        }

        loginViewModel.isPasswordValid.observe(this){
            loginViewModel.setAreFieldsValid()
        }

        loginViewModel.isEmailValid.observe(this){
            loginViewModel.setAreFieldsValid()
        }

    }

    private fun moveToSearchActivity(){
        finish()
        startActivity(Intent(this, SearchActivity::class.java))
    }

    private fun changeButtonColorsScheme(isEnabled: Boolean){
        if(isEnabled) {
            getColor(R.color.cyan).let {
                loginDataBinding.loginButton.setBackgroundColor(
                    it
                )
            }
        } else {
            getColor(R.color.steel_grey).let {
                loginDataBinding.loginButton.setBackgroundColor(
                    it
                )
            }
        }
    }

    private fun setListenersToEmailField(){
        loginDataBinding.emailEditText.editText?.doAfterTextChanged {
            removeWarning(loginDataBinding.emailEditText)
            validateEmail(loginDataBinding.emailEditText.editText?.text.toString())
            loginViewModel.setShouldShowInvalidFieldsWarning()
        }

        loginDataBinding.emailEditText.editText?.setOnFocusChangeListener { _, hasFocus ->
            if(!hasFocus) {
                loginViewModel.isEmailValid.value.let {
                    if (it == false)
                        addWarning(loginDataBinding.emailEditText, "Email Inválido")
                }
            }
        }
    }

    private fun setListenersToPasswordField(){
        loginDataBinding.passwordEditText.editText?.doAfterTextChanged {
            validatePassword(loginDataBinding.passwordEditText.editText?.text.toString())
            removeWarning(loginDataBinding.passwordEditText)
            loginViewModel.setHasActiveWarningInPassword(false)
            loginViewModel.setShouldShowInvalidFieldsWarning()
        }

        loginDataBinding.passwordEditText.editText?.setOnFocusChangeListener { _, hasFocus ->
            if(!hasFocus)
                loginViewModel.isPasswordValid.value.let {
                    if(it == false){
                        loginDataBinding.passwordEditText.endIconMode = END_ICON_NONE
                        addWarning(loginDataBinding.passwordEditText, "Senha Inválida")
                        loginViewModel.setHasActiveWarningInPassword(true)
                    }
                }
        }
    }

    private fun addWarning(layout: TextInputLayout?,warning: String){
        if(layout != null){
            layout.error = warning
            layout.editText?.error = warning
        }
    }

    private fun removeWarning(layout: TextInputLayout?){
        if(layout != null){
            layout.error = null
            layout.editText?.error = null
        }
    }

    private fun validateEmail(email: String){
        if(email.endsWith(".com") || email.endsWith(".com.br") && email.contains("@") && email.length > 10)
            loginViewModel.setIsEmailValid(true)
         else
            loginViewModel.setIsEmailValid(false)

    }

    private fun validatePassword(password: String){
        if(password.length >= 6)
            loginViewModel.setIsPasswordValid(true)
        else
            loginViewModel.setIsPasswordValid(false)
    }

    private fun createUser() : User{
        return User(loginDataBinding.emailEditText.editText?.text.toString(),
            loginDataBinding.passwordEditText.editText?.text.toString())
    }
}