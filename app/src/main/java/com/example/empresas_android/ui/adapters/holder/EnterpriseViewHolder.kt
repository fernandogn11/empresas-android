package com.example.empresas_android.ui.adapters.holder

import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.empresas_android.Constants.BASE_URL_TO_GLIDE
import com.example.empresas_android.databinding.EnterpriseItemBinding
import com.example.empresas_android.models.Enterprise

class EnterpriseViewHolder(private val itemBinding: EnterpriseItemBinding) : RecyclerView.ViewHolder(itemBinding.root) {

    fun bind(enterprise: Enterprise){
        Glide.with(itemBinding.root)
            .load(BASE_URL_TO_GLIDE + enterprise.photoUrl)
            .into(itemBinding.enterpriseImageView)
        itemBinding.enterpriseDescriptionTextView.text = enterprise.enterpriseType?.enterpriseTypeName
        itemBinding.enterpriseCountryTextView.text = enterprise.country
        itemBinding.enterpriseTitleTextView.text = enterprise.enterpriseName
    }

}