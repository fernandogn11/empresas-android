package com.example.empresas_android.ui.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.empresas_android.R
import com.example.empresas_android.databinding.EnterpriseItemBinding
import com.example.empresas_android.models.Enterprise
import com.example.empresas_android.ui.adapters.holder.EnterpriseViewHolder

class EnterpriseAdapter(private val dataSet : List<Enterprise>) : RecyclerView.Adapter<EnterpriseViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EnterpriseViewHolder {
        val binding = EnterpriseItemBinding.inflate(LayoutInflater.from(parent.context))

        return EnterpriseViewHolder(binding)
    }

    override fun onBindViewHolder(holder: EnterpriseViewHolder, position: Int) {
        holder.bind(dataSet[position])
    }

    override fun getItemCount(): Int {
       return dataSet.size
    }
}