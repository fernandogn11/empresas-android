package com.example.empresas_android.ui.search

import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.view.isGone
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.example.empresas_android.R
import com.example.empresas_android.databinding.ActivitySearchBinding
import com.example.empresas_android.models.Enterprise
import com.example.empresas_android.ui.adapters.EnterpriseAdapter


class SearchActivity : AppCompatActivity() {

    private val viewModel: SearchViewModel by viewModels()

    private lateinit var searchDataBinding : ActivitySearchBinding

    private var enterpriseList = listOf<Enterprise>()

    override fun onCreate(savedInstanceState: Bundle?) {

        searchDataBinding = ActivitySearchBinding.inflate(layoutInflater)

        setContentView(searchDataBinding.root)
        setupActionBar()
        createObservers()
        super.onCreate(savedInstanceState)
    }

    private fun setupActionBar(){
        supportActionBar?.title = null
        supportActionBar?.setDisplayShowHomeEnabled(false)
        supportActionBar?.setHomeButtonEnabled(false); // disable the button
        supportActionBar?.setDisplayHomeAsUpEnabled(false); // remove the left caret
        supportActionBar?.setDisplayShowHomeEnabled(false);
    }

    private fun createObservers(){
        viewModel.enterpriseList.observe(this){
            it?.let {
                searchDataBinding.searchRecyclerView.adapter = EnterpriseAdapter(it)
                if(it.isEmpty()){
                    searchDataBinding.errorTextView.visibility = VISIBLE
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_search, menu)
        with(menu?.findItem(R.id.searchMenu)?.actionView as SearchView){
            this.queryHint = "Pesquisar"

            this.setOnQueryTextListener(object: SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    query?.let {
                        viewModel.handleSearchRequest(it)
                        return true
                    }
                    return false
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    searchDataBinding.searchTextView.visibility = GONE
                    searchDataBinding.errorTextView.visibility = GONE
                    return true
                }

            })
        }
        return super.onCreateOptionsMenu(menu)
    }
}