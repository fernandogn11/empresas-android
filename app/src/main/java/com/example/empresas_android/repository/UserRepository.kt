package com.example.empresas_android.repository

import android.util.Log
import com.example.empresas_android.Constants.ACCESS_TOKEN
import com.example.empresas_android.Constants.CLIENT
import com.example.empresas_android.Constants.UID
import com.example.empresas_android.models.AuthenticatedUser
import com.example.empresas_android.models.User
import com.example.empresas_android.models.LoginResponse
import com.example.empresas_android.network.LoginService
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import retrofit2.*


class UserRepository : KoinComponent {

    private val loginService: LoginService by inject()

    suspend fun login(user: User): AuthenticatedUser? {
        Log.d("Fernando", loginService.toString())

        val response = loginService.login(user).awaitResponse()

        return if (response.isSuccessful) {
            AuthenticatedUser(
                response.headers()[ACCESS_TOKEN].toString(),
                response.headers()[CLIENT].toString(),
                response.headers()[UID].toString()
            )
        } else {
            null
        }
    }
}