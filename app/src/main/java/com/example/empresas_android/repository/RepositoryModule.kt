package com.example.empresas_android.repository

import org.koin.dsl.module

val repositoryModule = module{
    single{ UserRepository() }

    single { SearchRepository() }
}