package com.example.empresas_android.repository

import com.example.empresas_android.models.Enterprises
import com.example.empresas_android.network.SearchService
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import retrofit2.awaitResponse

class SearchRepository : KoinComponent {
    private val searchService : SearchService by inject()

    suspend fun search(query: String) : Enterprises? {
        val response = searchService.search(enterpriseName = query).awaitResponse()

        return if(response.isSuccessful){
            return response.body()
        } else {
            null
        }
    }
}