package com.example.empresas_android.repository.interceptor

import com.example.empresas_android.App
import com.example.empresas_android.App.Companion.getAccessToken
import com.example.empresas_android.App.Companion.getClient
import com.example.empresas_android.App.Companion.getUid
import com.example.empresas_android.Constants.ACCESS_TOKEN
import com.example.empresas_android.Constants.CLIENT
import com.example.empresas_android.Constants.UID
import okhttp3.Interceptor
import okhttp3.Response

class HeaderInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val requestBuilder = chain.request().newBuilder()

        if(getAccessToken() != null && getClient() != null && getUid() != null){
            requestBuilder.header(ACCESS_TOKEN, getAccessToken()!!)
                .header(CLIENT, getClient()!!)
                .header(UID, getUid()!!)
        }

        return chain.proceed(requestBuilder.build())
    }
}