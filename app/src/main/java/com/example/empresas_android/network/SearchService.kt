package com.example.empresas_android.network

import com.example.empresas_android.Constants.SEARCH_URL
import com.example.empresas_android.models.Enterprises
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface SearchService {
    @GET(SEARCH_URL)
    fun search(@Query("name") enterpriseName: String) : Call<Enterprises>
}