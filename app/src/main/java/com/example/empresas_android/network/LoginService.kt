package com.example.empresas_android.network

import com.example.empresas_android.Constants.SIGN_IN_URL
import com.example.empresas_android.models.User
import com.example.empresas_android.models.LoginResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface LoginService {
    @POST(SIGN_IN_URL)
    fun login(@Body user: User): Call<LoginResponse>
}