package com.example.empresas_android

import android.app.Application
import android.content.Context
import android.util.Log
import com.example.empresas_android.Constants.ACCESS_TOKEN
import com.example.empresas_android.Constants.CLIENT
import com.example.empresas_android.Constants.UID
import com.example.empresas_android.di.apiModule
import com.example.empresas_android.models.AuthenticatedUser
import com.example.empresas_android.repository.repositoryModule
import org.koin.core.context.startKoin

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        Log.d("Fernando", "app created")
        startKoin {
            modules(listOf(
                apiModule,
                repositoryModule
            ))
        }
        app = this
    }
    companion object{
        lateinit var app: App

        lateinit var headerMap: Map<String, String>

        val authUserPrefs by lazy {
            app.getSharedPreferences("AuthenticatedUserPrefs", Context.MODE_PRIVATE)
        }

        fun saveAuthenticatedUserData(userData: AuthenticatedUser){
            with(authUserPrefs.edit()){
                putString(ACCESS_TOKEN, userData.accessToken)
                putString(UID, userData.uid)
                putString(CLIENT, userData.client)
                apply()
            }
            initializeMap(userData)
        }

        fun initializeMap(userData: AuthenticatedUser){
            val map = mutableMapOf<String,String>()
            map[ACCESS_TOKEN] = userData.accessToken
            map[UID] = userData.uid
            map[CLIENT] = userData.client
            headerMap = map
        }

        fun getAccessToken() : String? {
            return authUserPrefs.getString(ACCESS_TOKEN, null)
        }

        fun getUid() : String? {
            return authUserPrefs.getString(UID, null)
        }

        fun getClient() : String? {
             return authUserPrefs.getString(CLIENT, null)
        }


    }
}
