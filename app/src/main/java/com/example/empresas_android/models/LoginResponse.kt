package com.example.empresas_android.models

import android.provider.ContactsContract
import com.example.empresas_android.Constants.ACCESS_TOKEN
import com.example.empresas_android.Constants.CLIENT
import com.example.empresas_android.Constants.UID
import com.google.gson.annotations.SerializedName


data class LoginResponse(
    @SerializedName("investor")
    val investor: Investor? = null,
    @SerializedName("enterprise")
    val enterprise: String? = null,
    @SerializedName("success")
    val success: Boolean? = null
)