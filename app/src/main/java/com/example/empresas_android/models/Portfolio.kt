package com.example.empresas_android.models

import com.google.gson.annotations.SerializedName

data class Portfolio(
    @SerializedName("enterprises_number")
    val enterprisesNumber: Int? = null,

    @SerializedName("enterprises")
    val enterprises: List<Enterprise> = listOf(),
)
