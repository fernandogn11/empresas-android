package com.example.empresas_android.models

import com.google.gson.annotations.SerializedName

data class EnterpriseType (
    @SerializedName("id")
    val id: Int? = null,

    @SerializedName("enterprise_type_name")
    val enterpriseTypeName: String? = null
)

