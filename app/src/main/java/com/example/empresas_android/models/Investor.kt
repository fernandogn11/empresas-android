package com.example.empresas_android.models

import com.google.gson.annotations.SerializedName

data class Investor(
    @SerializedName("id")
    val id: Int? = null,
    @SerializedName("investor_name")
    val investor_name: String? = null,
    @SerializedName("city")
    val city: String? = null,
    @SerializedName("country")
    val country: String? = null,
    @SerializedName("balance")
    val balance: Int? = null,
    @SerializedName("photo")
    val photo: String? = null,
    @SerializedName("portfolio")
    val portfolio: Portfolio? = null,
    @SerializedName("portfolio_value")
    val portifolio_value: Double? = null,
    @SerializedName("first_access")
    val firstAccess: Boolean? = null,
    @SerializedName("super_angel")
    val superAngel: Boolean? = null
)