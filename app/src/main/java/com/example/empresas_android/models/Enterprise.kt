package com.example.empresas_android.models

import com.google.gson.annotations.SerializedName

data class Enterprise(
    @SerializedName("id")
    var id : Int? = null,
    @SerializedName("email_enterprise")
    var emailEnterprise : String? = null,
    @SerializedName("facebook")
    var facebookUrl : String? = null,
    @SerializedName("twitter")
    var twitterUrl : String? = null,
    @SerializedName("linkedin")
    var linkedinUrl : String? = null,
    @SerializedName("phone")
    var phoneNumber : String? = null,
    @SerializedName("own_enterprise")
    var ownEnterprise : Boolean? = null,
    @SerializedName("enterprise_name")
    var enterpriseName : String? = null,
    @SerializedName("photo")
    var photoUrl : String? = null,
    @SerializedName("description")
    var  description : String? = null,
    @SerializedName("city")
    var city : String? = null,
    @SerializedName("country")
    var country : String? = null,
    @SerializedName("value")
    var value : Int? = null,
    @SerializedName("share_price")
    val sharePrice : Double? = null,
    @SerializedName("enterprise_type")
    val enterpriseType: EnterpriseType? = null,
)
