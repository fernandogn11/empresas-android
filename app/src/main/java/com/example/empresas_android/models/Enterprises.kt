package com.example.empresas_android.models

import com.google.gson.annotations.SerializedName

data class Enterprises(
    @SerializedName("enterprises")
    var enterpriseList: List<Enterprise>? = null
)
