package com.example.empresas_android.models

import com.google.gson.annotations.SerializedName

data class AuthenticatedUser (val accessToken : String, val client: String,val uid : String)