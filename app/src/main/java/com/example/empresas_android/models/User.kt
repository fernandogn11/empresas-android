package com.example.empresas_android.models

import com.google.gson.annotations.SerializedName

data class User(
    @SerializedName("email")
    val email: String? = null,
    @SerializedName("password")
    val password: String? = null,
)