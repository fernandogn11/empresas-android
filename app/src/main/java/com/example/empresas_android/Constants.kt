package com.example.empresas_android

object Constants {
    const val BASE_URL = "https://empresas.ioasys.com.br/api/v1/"
    const val BASE_URL_TO_GLIDE = "https://empresas.ioasys.com.br/api/v1/"
    const val SIGN_IN_URL  = "users/auth/sign_in"
    const val SEARCH_URL = "enterprises"
    const val ACCESS_TOKEN = "access-token"
    const val CLIENT = "client"
    const val UID = "uid"
}