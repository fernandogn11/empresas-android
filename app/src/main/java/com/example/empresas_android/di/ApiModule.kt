package com.example.empresas_android.di

import com.example.empresas_android.Constants.BASE_URL
import com.example.empresas_android.models.User
import com.example.empresas_android.network.LoginService
import com.example.empresas_android.network.SearchService
import com.example.empresas_android.repository.interceptor.HeaderInterceptor
import okhttp3.OkHttpClient
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val apiModule = module {

    single{
        OkHttpClient.Builder()
            .addInterceptor(HeaderInterceptor())
            .build()
    }

    single {
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(get())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    single { get<Retrofit>().create(LoginService::class.java) }

    single { get<Retrofit>().create(SearchService::class.java) }
}
