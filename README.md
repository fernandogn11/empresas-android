![N|Solid](logo_ioasys.png)

# README #

Estes documento README tem como objetivo fornecer as informações necessárias para realização do projeto Empresas.

### O QUE FAZER ? ###

* Você deve realizar um fork deste repositório e, ao finalizar, enviar o link do seu repositório para a nossa equipe. Lembre-se, NÃO é necessário criar um Pull Request para isso.

### ESCOPO DO PROJETO ###

* Deve ser criado um aplicativo Android utilizando linguagem Java ou Kotlin com as seguintes especificações:
* Login e acesso de Usuário já registrado
	* Para o login usamos padrões OAuth 2.0. Na resposta de sucesso do login a api retornará 3 custom headers (access-token, client, uid);
	* Para ter acesso as demais APIS precisamos enviar esses 3 custom headers para a API autorizar a requisição;
* Listagem de Empresas
* Detalhamento de Empresas

### Informações Importantes ###

* Layout e recortes disponíveis no Zeplin (http://zeplin.io)
Login - teste_ioasys
Senha - ioasys123

* Integração disponível a partir de uma collection para Postman (https://www.getpostman.com/apps) disponível neste repositório.
* O `README.md` deve conter uma pequena justificativa de cada biblioteca adicionada ao projeto como dependência.
* O `README.md` deve conter tambem o que você faria se tivesse mais tempo.
* O `README.md` do projeto deve conter instruções de como executar a aplicação
* Independente de onde conseguiu chegar no teste é importante disponibilizar seu fonte para analisarmos.

### Dados para Teste ###

* Servidor: https://empresas.ioasys.com.br
* Versão da API: v1
* Usuário de Teste: testeapple@ioasys.com.br
* Senha de Teste : 12341234

### Dicas ###

* Para requisição sugerimos usar a biblioteca Retrofit
* Para download e cache de imagens use a biblioteca Glide
* Para parse de Json use a biblioteca GSON

### Bônus ###

* Testes unitários, pode usar a ferramenta que você tem mais experiência, só nos explique o que ele tem de bom.
* Usar uma arquitetura testável. Ex: MVP, MVVM, Clean, etc.
* Material Design
* Utilizar alguma ferramenta de Injeção de Dependência, Dagger, Koin e etc..
* Utilizar Rx, LiveData, Coroutines.
* Padrões de projetos


### Dependências Utilizadas ### 

* Glide
	*Carregamento de imagens nos Cards
* OkHttp
	* Utilizado para adicionar um header na requisição de busca
* Retrofit
	* Fazer requisições ao servidor
* Gson
	* Converter a response das requisições de JSON para uma data class
* Koin
	* Injeção de dependência
* Material UI
	* Desenvolver botôes com warning
* ConstraintLayout
	* Layouts mais precisos e flexíveis
* LiveData
	* Usar Live Data para armazenamento de dados observaveis
* ViewModel
	* Utilizado para criar uma arquitetura MVVM
* JUnit
	* Testes Unitários
* Espresso
	* Testes de UI

### O que eu faria se tivesse mais tempo? ###

* Durante o tempo hábil, planejei fazer testes unitários, de componente e de UI, porém não consegui.
* Fazer um sistema de reautenticação com o token recebido pela API, haja vista que há um tempo de expiração para o mesmo.
* Ao entrar no aplicativo, caso token do usuário ainda fosse válido, não seria necessário o relogin, redirecionando o usuário para a página de procura.
* Adicionar o logo da ioasys ao Toolbar, para fazer isso bastava criar uma toolbar custom e inserir o logo.
* Melhorar o sistema de validação de campos
* Utilizar mais o arquivo de Constants.


### Como executar o aplicativo? ###

* Ao iniciar, o usuário cairá na página de login e irá inserir seus dados.
	* Caso o email contenha @ e termine em .com ou .br, ele será considerado válido, caso não, um warning será mostrado no campo de email.
	* Caso a senha seja maior que 6, ela será considerado válido, caso não, um warning será mostrado no campo de senha.
	* O estado inicial do botão é desativado, ele só será ativado caso ambos campos estejam preenchidos com dados válidos.
* Para submeter, o usuário deverá clicar no botão, que só será liberado ao preencher o formulário com campos válidos.
	* Caso a requisição não tenha sucesso, um warning será mostrado abaixo dos campos e o botão de submissão será bloqueado até que sejam alterados.
	* Caso a requisição tenha sucesso, o usuário será redirecionado para a tela de procura.
* Na tela de procura, o usuário deverá clicar no botão de procura, como indicado no aviso na tela
	* Ao clicar no botão de procura, esse aviso será removido da tela.
* Na caixa de procura, ao fazer a procura, as empresas que tiverem nomes parecidos com o que foi procurado, serão mostrados em cards na tela.
	* Caso a procura não retorne nada, um aviso será mostrado.
